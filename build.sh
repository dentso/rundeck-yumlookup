#!/bin/bash -x

option=$1
progname=yumlookup
config=`ls -1d bin/${progname}.conf | head -1`
if [ -z "$config" -a ! -f "$config" ]; then
  echo "ERROR: config file $config not found."
  exit 1
fi
version=`egrep "^version:" $config | cut -d: -f2`
binprog=${progname}-${version}
prefix=`pwd`
rpmbuilddir=${prefix}/rpmbuild
installbin=${prefix}/bin/${progname}.py
installinit=${prefix}/bin/${progname}
installconf=${prefix}/bin/${progname}.conf

PATH=$PATH:.

if [ "$option" = "-d" -a -n "${rpmbuilddir}" ]; then
  echo "Deleting old directory tree"
  rm -rf ${rpmbuilddir}
fi


if [ "$option" = "-c" -a -n "${rpmbuilddir}" ]; then
  echo "Installing CherryPy"
  sudo pip install CherryPy
  exit 0
fi

mkdir ${rpmbuilddir}
cat <<EOF >~/.rpmmacros
%_topdir   `pwd`/rpmbuild
%_tmppath  %{_topdir}/tmp
%_bindir   /usr/local/bin
EOF

echo "Create tarball for the project"
cd ${rpmbuilddir}
mkdir ${binprog} BUILD BUILDROOT RPMS SRPMS SOURCES SPECS
mkdir -p ${binprog}/usr/local/bin ${binprog}/etc/rc.d/init.d
install -m 644 ${installbin} ${binprog}/usr/local/bin
install -Dp -m0755 ${installinit} ${binprog}/etc/rc.d/init.d 
install -m 644 ${installconf} ${binprog}/etc
tar -zcvf ${binprog}.tar.gz ${binprog}

# copy to the sources dir
echo "cp ${binprog}.tar.gz SOURCES/"
cp ${binprog}.tar.gz SOURCES/
[ -n "${binprog}" ] && rm -rf ${binprog}*

echo "Generating SPEC file"
cat <<EOF > SPECS/${progname}.spec
# Don't try fancy stuff like debuginfo, which is useless on binary-only
# packages. Don't strip binary too
# Be sure buildpolicy set to do nothing
%define        __spec_install_post %{nil}
%define          debug_package %{nil}
%define        __os_install_post %{_dbpath}/brp-compress

Summary: A very simple toy bin rpm package
Name: yumlookup
Version: %{version}
Release: 1
License: GPL+
Group: Development/Tools
SOURCE0 : %{name}-%{version}.tar.gz
URL: <N/A>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description
%{summary}

%prep
%setup -q

%build
# Empty section.

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}

# in builddir
cp -a * %{buildroot}

%clean
rm -rf %{buildroot}

%post
/sbin/chkconfig --add yumlookup
/sbin/service yumlookup start > /dev/null 2>&1
%preun
if [ "$1" = 0 ] ; then
/sbin/service yumlookup stop > /dev/null 2>&1
/sbin/chkconfig --del yumlookup
fi
exit 0

%files
%defattr(-,root,root,-)
%defattr(755, root, root)
%{_bindir}/*
%{_initddir}/*
%config(noreplace) %{_sysconfdir}/*.conf

%changelog
* Thu Apr 1 2016  Esaie Vermilus <esaie.vermilus@hotschedules.com> %{version}-1
* Thu Apr 14 2016  Denise Stockman <denise.stockman@hotschedules.com> 1.0-0
- First Build

EOF

perl -pi -e "s/\%{version}/${version}/g" SPECS/${progname}.spec

#build
echo "Building rpm..."
pwd
rpmbuild -ba SPECS/${progname}.spec
newrpm=`ls -1d RPMS/x86_64/yumlookup-${version}-1.x86_64.rpm | head -1`

if [ -z "${newrpm}" ]; then
  echo "ERROR: rpm not found."
  exit 5
else 
  echo "Newly created rpm: ${newrpm}"
fi
cp ${newrpm} ${prefix}/bin


#register
if [ "$option" = "-p" -a -n "${newrpm}" ]; then
   echo "Registering rpm..."
   sudo /opt/infra-tools/bin/rpm-publish ${newrpm}
fi

exit 0
