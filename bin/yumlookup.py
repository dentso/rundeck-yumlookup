#!/usr/bin/python

import cherrypy, os, sys, yum, json
from cherrypy import tools

yb = yum.YumBase()
yb.conf.cache = os.geteuid() != 1
cherrypy.config.update({'server.socket_host': '0.0.0.0'})

class PackageQuery():
  @cherrypy.tools.json_out()
  @cherrypy.tools.json_in()
  @cherrypy.expose

  def findPackage(self, packageName):
    foundPkgs = list()
    pl = yb.doPackageLists(patterns=[packageName])
    if pl.installed:
      for pkg in sorted(pl.installed):
        foundPkgs.append({'name': str(pkg), 'value': str(pkg)})
    if pl.available:
      for pkg in sorted(pl.available):
        foundPkgs.append({'name': str(pkg), 'value': str(pkg)})
    if pl.reinstall_available:
      for pkg in sorted(pl.reinstall_available):
        foundPkgs.append({'name': str(pkg), 'value': str(pkg)})

    return foundPkgs

if __name__ == '__main__':
  cherrypy.quickstart(PackageQuery())

